//Jimmy Le 1936415
package tests;


import utilities.MatrixMethod;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	void lengthTests() {
		//2x3 array
		int[][] arrayTest = {{1,2,3} , {4,5,6}};
		int [][] dupArray = MatrixMethod.duplicate(arrayTest);
		assertEquals(2, dupArray.length);
		
		assertEquals(6,dupArray[0].length);
		
		//3x4 array
		int[][] arrayTest2 = {{1,2,3,4} , {4,5,6,7}, {7,8,9,0}};
		int [][] dupArray2 = MatrixMethod.duplicate(arrayTest2);
		assertEquals(3, dupArray2.length);
		
		assertEquals(8,dupArray2[0].length);
		
		//3x3 array
		int[][] arrayTest3 = {{1,2,3} , {4,5,6},{7,8,9}};
		int [][] dupArray3 = MatrixMethod.duplicate(arrayTest3);
		assertEquals(3, dupArray3.length);
		
		assertEquals(6,dupArray3[0].length);
		
	}
	
	@Test
	void valueTests() {
		//2x3 array
		int[][] arrayTest = {{1,2,3} , {4,5,6}};
		int [][] dupArray = MatrixMethod.duplicate(arrayTest);
		int value1 = dupArray[0][4];
		int value2 = dupArray[1][5];
		
		assertEquals(2, value1);
		
		assertEquals(6, value2);
		
		//3x4 array
		int[][] arrayTest2 = {{1,2,3,4} , {4,5,6,7}, {7,8,9,0}};
		int [][] dupArray2 = MatrixMethod.duplicate(arrayTest2);
		int value3 = dupArray2[1][6];
		int value4 = dupArray2[2][7];
		assertEquals(6, value3);
		
		assertEquals(0,value4);
		
		//3x3 array
		int[][] arrayTest3 = {{1,2,3} , {4,5,6},{7,8,9}};
		int [][] dupArray3 = MatrixMethod.duplicate(arrayTest3);
		int value5 = dupArray3[1][1];
		int value6 = dupArray3[2][5];
		assertEquals(5, value5);
		
		assertEquals(9,value6);
		
		//1x3 array
		int[][] arrayTest4 = {{1,2,3}};
		int [][] dupArray4 = MatrixMethod.duplicate(arrayTest4);
		int value7 = dupArray4[0][5];
		assertEquals(3, value7);
		
		//3x1
		int[][] arrayTest5 = {{1} , {4},{7}};
		int [][] dupArray5 = MatrixMethod.duplicate(arrayTest5);
		int value8 = dupArray5[1][1];
		int value9 = dupArray5[2][1];
		assertEquals(4, value8);
		
		assertEquals(7,value9);
		
		
	}

}
