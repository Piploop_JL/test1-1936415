//Jimmy Le 1936415
package tests;
import vehicles.Car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {
	
	
	
	@Test
	void speedTest() {
		
		Car testCar = new Car(1);
		int speed = testCar.getSpeed();
		assertEquals(1, speed);
		
	}
	

	@Test
	void constructorTest() {
		//positive speed test
		Car testCar = new Car(5);
		assertEquals(5, testCar.getSpeed());
		
		
		//negative speed test
		try {
			Car testCar2 = new Car(-4);
			
			fail("Constructor should have thrown an exception");
		}
		catch(IllegalArgumentException e){
			
		}
		
		
		//0 speed test	
		Car testCar3 = new Car(0);
		assertEquals(0, testCar3.getSpeed());
	
			
		
	}
	
	
	
	@Test
	void positionTest() {
		
		//current position test
		Car testCar = new Car(2);
		assertEquals(0,testCar.getLocation());		
		
	}
	
	@Test
	void moveRightTest() {
		//Move right
		Car testCar = new Car(2);
		testCar.moveRight();
		assertEquals(2,testCar.getLocation());	
		
		//Move right with 0 speed
		Car testCar2 = new Car(0);
		testCar.moveRight();
		assertEquals(0,testCar2.getLocation());	
		
	}
	
	@Test
	void moveLeftTest() {
		//move left
		Car testCar = new Car(3);
		testCar.moveLeft();
		assertEquals(-3, testCar.getLocation());
		//move left with 0 speed
		Car testCar2 = new Car(0);
		testCar.moveLeft();
		assertEquals(0,testCar2.getLocation());	
	}
	
	@Test
	void accelerationTest() {
		//accelerate
		Car testCar = new Car(3);
		testCar.accelerate();
		assertEquals(4, testCar.getSpeed());
		//accelerate on 0 speed
		Car testCar2 = new Car(0);
		testCar2.accelerate();
		assertEquals(1, testCar2.getSpeed());
		
		
	}
	
	
	@Test
	void decelerationTest() {
		//decelerate
		Car testCar = new Car(2);
		testCar.decelerate();
		assertEquals(1, testCar.getSpeed());
		//decelerate on 0 speed
		Car testCar2 = new Car(0);
		testCar2.decelerate();
		assertEquals(0, testCar2.getSpeed());
		
		
	}
	
	
	

}


