//Jimmy Le 1936415
package utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MatrixMethod {

	public static int[][] duplicate(int[][] startingArray){
		
		int length1 = startingArray.length;
		//assuming the 2d arrays are all the same length
		int length2 = startingArray[0].length;
		
		int newLength2 = length2*2;
		
		int[][] duplicatedArray = new int[length1][newLength2];
		
		duplicatedArray = fill(startingArray, duplicatedArray, length1, length2, newLength2);
		
		return duplicatedArray;
		
		

	}
	
	
	public static int[][] fill(int[][] firstArray, int[][] secondArray, int length1, int length2, int newLength2) {
		
		//1d array
		for(int i = 0; i < length1; i++) {
			int j = 0;
			//loops through the 2d of the new array
			while(j < newLength2) {
				
					for(int k = 0; k < length2; k++) {
						secondArray[i][j] = firstArray[i][k];
						
						j++;
					}
					
			}
			
		}		
		
		return secondArray;
	}
	
	
	/*public static void main(String[] args){
		int[][] arrayTest = {{1,2,3,4} , {4,5,6,7}, {7,8,9,0}};
		int [][] dupArray = MatrixMethod.duplicate(arrayTest);
		
		
		for(int i = 0; i < dupArray.length; i++) {
			
			for (int j = 0; j < dupArray[i].length; j++) {
				System.out.println(dupArray[i][j]);
			}
		}
	
		
	}*/
	

}
